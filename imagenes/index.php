<?php include_once '../require/head.php'; ?>
    <article class="" style="margin: 1% 3% 0 3%;">
      <div class="row">
      <div class="niveles">
        <h2 class="left-align"><i class="fa fa-camera" aria-hidden="true"></i> Galeria de Fotos</h2>
      </div>
      </div>
      <div class="albums">
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/img/maternal.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/img/maternal.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/img/maternal.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/storage/0_mg7510-800_c77f8e5e.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/storage/0_mg7510-800_c77f8e5e.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>


          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/storage/0_mg7510-800_c77f8e5e.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/storage/0_mg7510-800_c77f8e5e.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/storage/0_mg7510-800_c77f8e5e.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/storage/0_mg7510-800_c77f8e5e.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/storage/0_mg7510-800_c77f8e5e.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
          <div class="col-20 borde-azul">
            <a class="directorio" href="/niveles/maternal">
              <div class="galeria">
                <img class="responsive-img" data-original="/storage/0_mg7598-800_aba188c3.jpg" alt="Maternal">
                <span>Ver Fotos</span>
                <i class="icon-camera fa fa-camera" aria-hidden="true"></i>
              </div>
              <div class="galeria-text">
                <h5 class="center-align">Galeria de Fotos 1</h5>
              </div>
            </a>
          </div>
      </div>
    </article>
<?php include_once '../require/footer.php'; ?>
