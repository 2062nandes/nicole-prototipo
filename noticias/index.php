<?php include_once '../require/head.php'; ?>
    <article class="" style="margin: 1% 3% 0 3%;">
      <div class="row">
      <div class="niveles">
        <h2 class="left-align"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Noticias</h2>
      </div>
      </div>
      <div class="albums">
        <div class="col-20 card small">
          <div class="card-image">
              <img data-original="http://static01.heraldo.es/uploads/imagenes/8col/2016/12/22/_obcb3e0_ae260158.jpg?c847ff98137f13d58377dee8cbccd8af">
              <a href=""><span class="card-title">¿Existe una alternativa al castigo para educar a nuestros hijos?</span></a>
            </div>
            <div class="card-content">
              <p>Si recurrimos únicamente al castigo, tan solo estamos evitando un problema, ya que solo sancionamos una conducta no deseada sin enseñarles, a cambio, otra apropiada.</p>
            </div>
            <div class="center-align card-action">
              <a class="btn btn-leer" href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Leer más</a>
            </div>
        </div>
        <div class="col-20 card small">
          <div class="card-image">
              <img data-original="https://ep01.epimg.net/economia/imagenes/2016/01/22/actualidad/1453461456_561424_1453466640_noticia_normal_recorte1.jpg">
              <a href=""><span class="card-title">George Kembel: “Se aprende haciendo, y no escuchando a un profesor”</span></a>
            </div>
            <div class="card-content">
              <p>El cofundador de la d.school de Stanford cuenta cómo funciona la escuela que ha revolucionado la metodología de la enseñanza</p>
            </div>
            <div class="center-align card-action">
              <a class="btn btn-leer" href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Leer más</a>
            </div>
        </div>
        <div class="col-20 card small">
          <div class="card-image">
              <img data-original="/img/maternal.jpg">
              <span class="card-title">Card Title  jajaj jajaja jajaja jajaja prueba de sonifiodo kjsdsd </span>
            </div>
            <div class="card-content">
              <p>I am a very simple card. I am good at containing small bits of information.
              I am convenient because I require little markup to use effectively.</p>
            </div>
            <div class="center-align card-action">
              <a class="btn btn-leer waves-effect waves-light" href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Leer más</a>
            </div>
        </div>
        <div class="col-20 card small">
          <div class="card-image">
              <img data-original="/img/maternal.jpg">
              <span class="card-title">Card Title  jajaj jajaja jajaja jajaja prueba de sonifiodo kjsdsd </span>
            </div>
            <div class="card-content">
              <p>I am a very simple card. I am good at containing small bits of information.
              I am convenient because I require little markup to use effectively.</p>
            </div>
            <div class="center-align card-action">
              <a class="btn btn-leer waves-effect waves-light" href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Leer más</a>
            </div>
        </div>
      </div>
    </article>
<?php include_once '../require/footer.php'; ?>
