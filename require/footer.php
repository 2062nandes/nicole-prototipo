  <footer class="dir-fot">
    <div class="container">
      <div class=  "row" style="margin-bottom: 5px;">
      <div class="col l12 m12 s12">
      <div class="fwrap">
      <div id="datos">
        <p class="center-align"> <strong style="font-weight:800;">Dirección:</strong> La Paz-Bolivia, Z. Miraflores, Pasoskanqui, esq, Estados Unidos Nº 1469 |	<a href="contactos#ubiquenos"><i style="font-size: 1.2em" class="fa fa-map-marker"> </i> Ver-mapa</a></p>
        <p class="center-align"><i style="font-size: 1.2em" class="fa fa-phone"></i>Teléfonos: 2223112 - 2228763</p>
      </div>
      </div>
      </div>
      <div class="col s12 m12 l12 center-align">
      <ul class="social_icons">
      <li><a target="_blank" href="https://www.facebook.com/Centro-Educativo-Nicole-139508483158668"><i class="waves-effect waves-light fa fa-facebook"></i></a></li>
      <li>&nbsp;&nbsp; <i style="font-size: 1.2em" class="fa fa-envelope"></i>info@centroeducativonicole.edu.bo</li>
      </ul>
      </div>
      </div>
    </div>
  </footer>
  <footer class="barra-fot">
  <div class="container">
  <div class="row barra" style="margin-bottom: 0;">
  <div class="col s12 l7 center-align">
  Todos los Derechos Reservados &REG; <strong>Centro Educativo Nicole.</strong> &COPY; 2017.
  </div>
  <div class="col s12 l5 center-align" style="color: rgba(255, 255, 255, 0.22);">
  <p class="ah"><a href="//ahpublic.com" target="_blank" id="ahpublicidad">Diseño y Programación Ah! Publicidad</a></p>
  </div>
  </div>
  </div>
</footer>
<div id="wowslider-container1" style="display:none;"></div>
<script src="/assets/6d1bea28/engine1/jquery.js"></script>
<script src="/assets/6d1bea28/vendor/materialize/materialize.js"></script>
<script src="/assets/6d1bea28/js/jquery.lazyload.js"></script>
<script src="/assets/6d1bea28/js/wow.min.js"></script>
<script src="/assets/6d1bea28/vendor/fancybox/jquery.fancybox.min.js"></script>
<script src="/assets/6d1bea28/js/baguetteBox.min.js"></script>
<script src="/assets/6d1bea28/engine1/wowslider.js"></script>
<script src="/assets/6d1bea28/engine1/script.js"></script>
<script src="/assets/6d1bea28/js/main.js"></script>        <script type="text/javascript">
$(document).ready(function(){
baguetteBox.run('.gallery');
baguetteBox.run('.galeria');
$('.carousel.gallery').carousel();
$('.carousel.gallery').carousel({dist:0});
window.setInterval(function(){$('.carousel.gallery').carousel('next')},3500)
});
</script>
</body>
</html>
