<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="designer" content="Fernando Javier Averanga Aruquipa"/>
        <meta name="author" content="" />
        <meta name="keywords" content="" />
        <!-- <meta name="description" content="Jardín infantil, parvulario, guardería,  prekinder, kinder. Centro de educación inicial. Transporte de puerta a puerta." /> -->
        <title>Centro Educativo NICOLE &mdash; Centro Educativo Nicole</title>
        <!--Let browser know website is optimized for mobile-->
        <!-- <link rel="icon" href="/favicon.png" type="image/png"> -->
        <meta name="csrf-param" content="_csrf">
<meta name="csrf-token" content="UVZtLl9IdWkAeytMMQs5GT1iWlYcAD4FOWUjSzoOOC8kDhxgCz82Cg==">
<meta name="og:title" content="Centro Educativo Nicole">
<meta name="og:type" content="website">
<meta name="description" content="Jardín infantil, parvulario, guardería,  prekinder, kinder. Centro de educación inicial. Transporte de puerta a puerta.">
<meta name="og:description" content="Jardín infantil, parvulario, guardería,  prekinder, kinder. Centro de educación inicial. Transporte de puerta a puerta.">
<link rel="shortcut icon" href="/favicon.ico">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Pangolin" rel="stylesheet">
<link href="/assets/6d1bea28/vendor/materialize/materialize.css" rel="stylesheet">
<link href="/assets/6d1bea28/css/font-awesome.min.css" rel="stylesheet">
<link href="/assets/6d1bea28/css/animate.css" rel="stylesheet">
<link href="/assets/6d1bea28/vendor/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="/assets/6d1bea28/css/style.css" rel="stylesheet">
<link href="/assets/6d1bea28/styles.css" rel="stylesheet">
<link href="/assets/6d1bea28/css/responsive-nicole.css" rel="stylesheet">
<link href="/assets/6d1bea28/engine1/style.css" rel="stylesheet">
<link href="/assets/6d1bea28/css/baguetteBox.css" rel="stylesheet">    </head>
    <!-- style="background:url(http://www.centroeducativonicole.edu.bo/images/background.jpg);background-repeat:repeat;" -->
    <body>
    <div class="row barra">
      <div class="right">
        <ul>
          <li><a href="/"><i class="fa fa-home" aria-hidden="true"></i> Inicio</a></li>
          <li><a href="contactos"><i class="fa fa-envelope" aria-hidden="true"></i> Contactos</a></li>
        </ul>
      </div>
      <div class="left"><h1 class="slogan"><strong>JARDÍN INFANTIL:</strong> MATERNAL - PARVULARIO (Guardería) - KINDER - PREKINDER</h1></div>
    </div>
    <header>
      <nav class="nav-extended" role="navigation">
        <div class="nav-wrapper row">
          <div class="col s5 m5 l5">
            <a id="logo-container" href="/" class="brand-logo wow fadeIn" data-wow-duration="2.9s" data-wow-delay="0.1s">
              <h1>Centro Educativo</h1>
              <h2>Nicole</h2>
            </a>
              <img class="sol-resplandor" src="/img/sol-resplandor.png" alt="">
              <img class="solsito" src="/img/sol-background.png" alt="">
              <img class="ninios" src="/img/nicole/nicole.png" alt="">
              <img class="ninia" src="/img/nicole/manito-ninia.png" alt="">
              <img class="ninio" src="/img/nicole/manito-ninia.png" alt="">
          </div>
          <div class="col s12">
            <ul><li class="abejita"><a><img class="animated fadeInLeft" src="/img/ave-bebe.gif" alt=""></a></li></ul>
          </div>
          <div class="menu-principal center">
              <ul>
                <li class="libuo"><a class="waves-effect waves-light btn-transparente wow rotateInDownLeft" href="/nicole" data-wow-duration="2.0s" data-wow-delay="0.5s">
                  <span class="azul">¿Quién es Nicole?</span>
                  <img class="cuerpo buo" src="/img/Buo-cuerpo.png" alt=""/>
                  <img class="parte-buo"src="/img/Buo-parte.png" alt=""/>
                </a></li>
                <li class="lipollito"><a class="waves-effect waves-light btn-transparente wow rotateInDownLeft" href="/imagenes" data-wow-duration="2.0s" data-wow-delay="0.9s">
                  <span class="amarillo" style="box-shadow:none"><span class="right" style="box-shadow:none">Imágenes</span> <i class="fa fa-camera" style="float:right;" aria-hidden="true"></i></span>
                  <img class="cuerpo huevo" style="width: 88px;" src="/img/huevo-cuerpo.png" alt=""/>
                  <img class="parte-huevo"src="/img/huevo-parte.png" alt=""/>
                </a></li>
                <li class="lipelicano"><a class="waves-effect waves-light btn-transparente wow rotateInDownLeft" href="/horarios" data-wow-duration="2.0s" data-wow-delay="0.1s">
                  <span class="verde">Horarios <i class="fa fa-clock-o" aria-hidden="true"></i></span>
                  <img class="cuerpo buo" src="/img/mono-cuerpo.png" alt=""/>
                  <img class="parte1-mono" src="/img/mono-parte1.png" alt=""/>
                  <img class="parte2-mono" src="/img/mono-parte2.png" alt=""/>
                </a></li>
                <li class="lipato"><a href="/calendario" class="waves-effect waves-light btn-transparente wow rotateInDownRight" data-wow-duration="2.0s" data-wow-delay="0.1s" style="z-index: 2;">
                  <span class="rosa" style="box-shadow:none"><span class="right" style="box-shadow:none">Calendario</span> <i class="fa fa-calendar" style="float:right;" aria-hidden="true"></i></span>
                  <img class="cuerpo huevo" src="/img/patos-cuerpo.png" alt=""/>
                  <img class="parte1-pato"src="/img/patos-parte1.png" alt=""/>
                  <img class="parte2-pato"src="/img/patos-parte2.png" alt=""/>
                </a></li>
                <li class="litortuga"><a href="/niveles" class="waves-effect waves-light btn-transparente wow rotateInDownRight" data-wow-duration="2.0s" data-wow-delay="0.5s">
                  <span class="celeste">Niveles <i class="fa fa-bar-chart"></i></span>
                  <img class="cuerpo buo" src="/img/pelicano-cuerpo.png" alt=""/>
                  <img class="parte-pelicano"src="/img/pelicano-parte.png" alt=""/>
                </a></li>
                <li class="ligirafa"><a href="/noticias" class="waves-effect waves-light btn-transparente wow rotateInDownRight" data-wow-duration="2.0s" data-wow-delay="0.9s" style="z-index: 2;">
                  <span class="rojo">Noticias <i class="fa fa-bell" aria-hidden="true"></i></span>
                  <img class="cuerpo buo" style="width: 110px;" src="/img/girafa-cuerpo.png" alt=""/>
                  <img class="cuerpo buo"src="/img/girafa-parte1.png" style="z-index: -1;width: 110px;" alt=""/>
                  <img class="parte-girafa"src="/img/girafa-parte2.png" alt=""/>
                </a></li>
              </ul>
          </div>
           <ul id="nav-mobile" class="side-nav" style="background:linear-gradient(rgba(255, 255, 255, 0.94) -1%, rgba(0, 156, 255, 0.69) 88%);">
                <img src="/img/nicole/site_logo.gif"  style="width: 55%; margin-left: 60px;margin-right: 60px;margin-top: 15px;margin-bottom: -15px;" alt="Centro Educativo Nicole">

                <div class="col s12">
                <li><a class="waves-effect waves-light btn-transparente" href="/nicole">
                  <span class="azul" style="text-align:left;">¿Quién es Nicole?</span>
                  <img class="cuerpo buo" src="/img/Buo-cuerpo.png" alt=""/>
                  <img class="parte-buo"src="/img/Buo-parte.png" alt=""/>
                </a></li>
                </div>
                <div class="col s12">
                <li><a class="waves-effect waves-light btn-transparente" href="/imagenes">
                  <span class="amarillo">Imágenes <i class="fa fa-camera" aria-hidden="true"></i></span>
                  <img class="cuerpo buo" style="width: 88px;" src="/img/huevo-cuerpo.png" alt=""/>
                  <img class="parte-huevo"src="/img/huevo-parte.png" alt=""/>
                </a></li>
                </div>
                <div class="col s12">
                <li><a class="waves-effect waves-light btn-transparente" href="/horarios">
                  <span class="verde">Horarios <i class="fa fa-clock-o" aria-hidden="true"></i></span>
                  <img class="cuerpo buo" src="/img/mono-cuerpo.png" alt=""/>
                  <img class="parte1-mono" src="/img/mono-parte1.png" alt=""/>
                  <img class="parte2-mono" src="/img/mono-parte2.png" alt=""/>
                </a></li>
                </div>
                <div class="col s12">
                <li><a href="/calendario" class="waves-effect waves-light btn-transparente">
                  <span class="rosa">Calendario <i class="fa fa-calendar" aria-hidden="true"></i></span>
                  <img class="cuerpo buo" src="/img/patos-cuerpo.png" alt=""/>
                  <img class="parte1-pato"src="/img/patos-parte1.png" alt=""/>
                  <img class="parte2-pato"src="/img/patos-parte2.png" alt=""/>
                </a></li>
                </div>
                <div class="col s12">
                <li><a href="/niveles" class="waves-effect waves-light btn-transparente">
                  <span class="celeste">Niveles <i class="fa fa-bar-chart"></i></span>
                  <img class="cuerpo buo" src="/img/pelicano-cuerpo.png" alt=""/>
                  <img class="parte-pelicano"src="/img/pelicano-parte.png" alt=""/>
                </a></li>
                </div>
                <div class="col s12">
                <li><a href="/noticias" class="waves-effect waves-light btn-transparente">
                  <span class="rojo">Noticias <i class="fa fa-bell" aria-hidden="true"></i></span>
                  <img class="cuerpo buo" style="width: 110px;" src="/img/girafa-cuerpo.png" alt=""/>
                  <img class="cuerpo buo"src="/img/girafa-parte1.png" style="z-index: -1;width: 110px;" alt=""/>
                  <img class="parte-girafa"src="/img/girafa-parte2.png" alt=""/>
                </a></li>
                </div>
           </ul>
        </div>
      </nav>
    <div class="container">
      <div style="width: 95%;margin: auto;">
        <a id="menu-movil" href="/#" data-activates="nav-mobile" class="col s12 button-collapse waves-effect waves-light btn-transparente wow fadeIn" data-wow-duration="2.0s" data-wow-delay="0.1s" style="height: 48px;">
           <span class="azul"><i style="position: absolute;left: 4%;top: 12px;" class="fa fa-bars fa-lg" aria-hidden="true"></i> MENÚ NICOLE</span>
         </a>
      </div>
    </div>
    </header>
