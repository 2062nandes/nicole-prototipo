'use strict';
  const os = require('os'),
        gulp = require('gulp'),

        rename = require('gulp-rename'),
        concurrentTransform = require('concurrent-transform'),
        changed = require('gulp-changed'),
        imagemin = require('gulp-imagemin'),
        pngquant = require('imagemin-pngquant'),
        imageResize = require('gulp-image-resize'),

        watch = require('gulp-watch'),
        postCss = require('gulp-postcss'),
        simpleVars = require('postcss-simple-vars'),
        nested = require('postcss-nested'),
        colorRgbaFallback = require('postcss-color-rgba-fallback'),
        opacity = require('postcss-opacity'),
        pseudoelements = require('postcss-pseudoelements'),
        vmin = require('postcss-vmin'),
        pixrem = require('pixrem'),
        willChange = require('postcss-will-change'),
        customMedia = require('postcss-custom-media'),
        mediaMinMax = require('postcss-media-minmax'),
        cssnano = require('cssnano'),
        pxtorem = require('postcss-pxtorem'),// Convertir valores de px a rem basado en baseFontSize
        cpus = os.cpus().length;

  var resizeAndMinImagesTask = function(options){
    options['imagemin'] = {
      progressive: true,
      interlaced: true,
      svgoPlugins: [{
        removeViewBox: false //https://github.com/svg/svgo/issues/3
      }],
      use: [
        pngquant()
      ]
    }
    return function () {
        return gulp.src("./public_html/srcimg/images/**/*.{jpg,jpeg,gif,png,svg}")
        .pipe(rename(function (path) {
          path.basename += "-" + options.width;
        }))
        .pipe(changed("./public_html/distimg/images"))
        .pipe(concurrentTransform(imageResize({width: options.width}), cpus))
        .pipe(imagemin(options["imagemin"]))
        .pipe(gulp.dest("./public_html/distimg/images"));
      }
  };
gulp.task("_MG_1521", resizeAndMinImagesTask({width: 800}));

gulp.task("_MG_2077", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2310", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2606", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1523", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1818", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2087", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2347", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2612", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1524", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1831", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2103", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2357", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2615", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1525", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1842", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2107", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2393", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2617 1", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1550", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1869", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2120", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2404", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2648", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1556", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1894", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2137", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2413", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2650", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1566", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1917", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2156", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2433", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1567", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2032", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2157", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2492", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2716", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1593", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2037", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2161", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2540", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2727", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1596", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2060", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2170", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2585", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2816", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1597", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2074", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2600", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2870", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1598", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2076", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2602", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2952", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_1745", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2077 (1)", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_2603", resizeAndMinImagesTask({width: 800}));
gulp.task("_MG_3061", resizeAndMinImagesTask({width: 800}));

  // gulp.task("isotipo-01", resizeAndMinImagesTask({width: 60}));
  // gulp.task("isotipo-02", resizeAndMinImagesTask({width: 60}));
  // gulp.task("isotipo-03", resizeAndMinImagesTask({width: 60}));
  // gulp.task("isotipo", resizeAndMinImagesTask({width: 60}));
  gulp.task("images",["_MG_1521",   "_MG_2077",  "_MG_2310",  "_MG_2606",
"_MG_1523",  "_MG_1818",      "_MG_2087",  "_MG_2347",  "_MG_2612",
"_MG_1524",  "_MG_1831",      "_MG_2103",  "_MG_2357",  "_MG_2615",
"_MG_1525",  "_MG_1842",      "_MG_2107",  "_MG_2393",  "_MG_2617 1",
"_MG_1550",  "_MG_1869",      "_MG_2120",  "_MG_2404",  "_MG_2648",
"_MG_1556",  "_MG_1894",      "_MG_2137",  "_MG_2413",  "_MG_2650",
"_MG_1566",  "_MG_1917",      "_MG_2156",  "_MG_2433",
"_MG_1567",  "_MG_2032",      "_MG_2157",  "_MG_2492",  "_MG_2716",
"_MG_1593",  "_MG_2037",      "_MG_2161",  "_MG_2540",  "_MG_2727",
"_MG_1596",  "_MG_2060",      "_MG_2170",  "_MG_2585",  "_MG_2816",
"_MG_1597",  "_MG_2074",      "_MG_2600",  "_MG_2870",  "_MG_1598",  "_MG_2076",  "_MG_2602",  "_MG_2952",
"_MG_1745",  "_MG_2077 (1)", "_MG_2603",  "_MG_3061"]);



  let postCssPlugins = [
    simpleVars,
    nested,
    cssnano({
      autoprefixer: {
        add:true
      },
      core: false
    }),
    colorRgbaFallback,
    opacity,
    pseudoelements,
    vmin,
    pixrem,
    willChange,
    customMedia,
    mediaMinMax,
    pxtorem
  ];
  gulp.task('styles', ()=>
    gulp.src('./assets/6d1bea28/src/*.css')
        .pipe(postCss(postCssPlugins))
        .pipe(gulp.dest('./assets/6d1bea28/css'))
  );

  gulp.task('watch', function () {
    gulp.watch("./public_html/srcimg/images/**/*.{jpg,jpeg,gif,png,svg}", ['images',]);
  });
  gulp.task('default', [], function(){
    gulp.watch('./assets/6d1bea28/src/*.css',['styles']);
  });
