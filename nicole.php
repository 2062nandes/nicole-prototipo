<?php include_once 'require/head.php'; ?>
<div class="row">
<article class="" style="margin: 0 25px 0 25px;">

    <div class="col s12 l8" style="float: right;">
      <h3>¿Quién es Nicole?</h3>
       <ul id="tabs-swipe-demo" class="tabs" style="border-radius: 26px 26px 0 0; background:white;overflow: hidden;">
         <li class="tab col s24"><a href="#historia" class="active">Historia</a></li>
         <li class="tab col s24"><a href="#mision">Misión</a></li>
         <li class="tab col s24"><a href="#vision">Visión</a></li>
         <li class="tab col s24"><a href="#objetivos">Objetivos</a></li>
         <li class="tab col s24"><a href="#personal">Personal</a></li>
       </ul>
       <div id="historia" class="col s12 ncl">
          <p>Se podría decir que el “CEN” nació legalmente un 24 de Noviembre de 1989, fundando sus primeros cimientos con la idea de proporcionar un servicio integral, en el amplio sentido de la palabra, a los niños y niñas de la ciudad de La Paz.</p>
  <p>Desde el nacimiento nuestros pilares se han basado en fomentar la independencia, la autonomía, el aprendizaje y la seguridad en sí mismos, en base a la confianza, calidad y convencimiento de nuestros servicios.</p>
  <p>Conocemos y sabemos que es de suma importancia rodear a nuestros niños y niñas de amor, respeto, alegrías, juegos, atención y fortalezas: pero, a su vez, es necesario trabajar las normas, reglas y limitaciones, con la intención de que todas ellas instauren una segura autoestima, independencia y autonomía, logrando así fortalecer la toma de decisiones de nuestros pequeños.</p>
  <p>Nosotros, como responsables del “CEN”, buscamos diariamente la explotación de todas sus capacidades, dando una prioridad a la creatividad, a la experimentación y al descubrimiento de nuevas experiencias.</p>
  <p>Como estrategia principal de enseñanza utilizamos el juego, orientándolo a métodos, estilos y formas de trabajo que permitan el canalizar inquietudes, intereses, virtudes y, a su vez, facilitar el logro de los objetivos planificados de enseñanza para cada ciclo de nuestros niños y niñas.</p>
  <p>Estamos seguros de que todo lo planificado y buscado por el “CEN” se conseguirá de forma exitosa con el apoyo del pilar más importante en la vida de nuestros pequeños: sus “Familias”; porque nuestros niños y niñas buscan constantemente la aprobación de sus familiares para reforzar su aprendizaje, descubrimientos, experiencias e inquietudes. Por eso, es indispensable trabajar coordinadamente entre “FAMILIA” Y “CEN”, para alcanzar el desarrollo integral que pretendemos.</p>
      </div>
       <div id="mision" class="col s12 ncl">
          <p>El “CEN” busca diariamente estimular por todos los medios posibles el desarrollo integral de nuestros niños y niñas, con la misión de formar personitas independientes, autónomas, seguras de si mismas y capaces de desenvolverse en una sociedad cambiante con respeto, amor, empatía y felicidad.</p>
      </div>
       <div id="vision" class="col s12 ncl">
          <p>Cuánta razón esgrime las palabras del colosal Picasso cuando escribe:</p>
  <p>“A los nueve años de edad, yo dibujaba como un miguel ángel maduro, sin embargo, toda mi vida he tratado de pintar como los niños, sin conseguirlo jamás”</p>
  <p>Tomando en cuenta estas profundas palabras, nuestra visión es lograr que los niños y niñas disfruten del ser niños/as, mejorando diariamente la calidad de tiempo que les dedicamos acomodándonos a los constantes cambios tanto educativos, como sociales y humanos, todo y más con el objetivo de brindar un mayor enriquecimiento en el desarrollo cognitivo, social y emocional de nuestros pequeños.</p>
      </div>
       <div id="objetivos" class="col s12 ncl">
          <p>Objetivo general.</p>
  <p>Desarrollar tanto las capacidades como las habilidades cognitivas, motrices, sociales y humanas, en base a programas anuales, mensuales y semanales de trabajo con una metodología propia de enseñanza, dirigidos a niños y niñas desde los ocho meses a los seis años de edad.</p>
  <p>Objetivos específicos.</p>
  <ul>
          <li>Estimular las capacidades y habilidades cognitivas, motrices, sociales y humanas.</li>
          <li>Desarrollar la independencia, autonomía, autoestima y la auto-confianza.</li>
          <li>Inculcar valores sociales y humanos.</li>
          <li>Fortalecer el lazo familia – niño/niña.</li>
          <li>Despertar habilidades artísticas y deportivas.</li>
      </ul>
       </div>
       <div id="personal" class="col s12 ncl">
          <p>El “CEN” cuenta con un personal formado y con una titulación requerida, que aspiran a conseguir el desarrollo integral de nuestros niños y niñas, distribuyendo los tiempos y espacios para cada etapa de desarrollo, logrando así que los niños/niñas adquieran una experiencia grata de aprendizaje y juego.</p>
       </div>
    </div>
    <div class="col s12 l4">
      <article style="margin: 0 25px 0 25px;">
        <h3>Horarios</h3>
        <div class="col s12" style="padding: 10px 0;">
          <p>Abrimos nuestras puertas a niños y niñas desde los ocho meses hasta los seis años de edad en los siguientes horarios:</p>
          <p><strong>Turno Mañana:</strong> 8:00 a 12:30 <br />
          <strong>Turno Tarde:</strong> 14:00 a 18:30</p>
          <div class="flex-center-group">
            <p>Servicios de transporte de puerta a puerta.</p>
            <img class="responsive-img" src="/img/transporte.png" alt="Transporte de puerta a puerta">
          </div>
        </div>
      </article>
    </div>
</article>
</div>
<?php include_once 'require/footer.php'; ?>
