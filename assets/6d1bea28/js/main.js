jQuery(function($) {'use strict',
	//Initiat WOW JS
	new WOW().init({
		mobile: false
	});
	$(document).ready(function(){
		$("img").lazyload({
			threshold : 150,
			effect : "fadeIn"
		});
		$(".button-collapse").sideNav();
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
  });
	$(document).ready(function(){
    $('ul.tabs').tabs({
			responsiveThreshold: Infinity, // breakpoint for swipeable
			swipeable: true
		});
  });
	$(document).ready(function(){
    $('.materialboxed').materialbox();
  });
	$(document).ready(function(){
		$('.carousel').carousel();
	});
	/*Formulario de contacto*/
  $(function(){
    $('input, textarea').each(function() {
      $(this).on('focus', function() {
        $(this).parent('.input').addClass('active');
     });
    if($(this).val() != '') $(this).parent('.input').addClass('active');
    });
  });

  var message = $('#statusMessage');
  $('.deletebtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "none");
    $("#theForm textarea").css("box-shadow", "none");
    $("#theForm select").css("box-shadow", "none");
    $(".input").removeClass("active");
  });
  $('.submitbtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su mensaje fue envíado correctamente,<br>le responderemos en breve");
        }
        else{
          message.html("Su mensaje no pudo ser envíado");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });
});

/* Modal Videos Youtube */
;(function() {
		//Youtube links & embedded videos
	$('[src*="youtube.com"], [href*="youtube.com"]').each(function () {
			var url = $(this).attr('src') ? $(this).attr('src') : $(this).attr('href');
			var galeriaText = $(this).attr('title');
			console.log('url: ' + url);
			var matchId = url.match(/(?:\/embed\/|\/watch\?v=)([a-z0-9]+\w*-*\w*_*\w*)/i);
			if (matchId) {
					console.log('matched id: ' + matchId[1]);
				$(this).replaceWith('<a class="galeria" href="https://www.youtube.com/watch?v=' + matchId[1] + '" data-modal="button"><img class="responsive-img" data-original="//img.youtube.com/vi/' + matchId[1] + '/0.jpg" alt="'+ galeriaText +'"><span>Reproducir vídeo</span><i class="icon-camera fa fa-youtube-play" aria-hidden="true"></i></a><div class="galeria-text"><a href="https://www.youtube.com/watch?v=' + matchId[1] + '" data-modal="button"><h5 class="center-align">'+ galeriaText +'</h5></a></div>');
			}
	});

  var Modal = function() {
    var prefix = 'Modal-';
    this.Class = {
      stopOverflow: prefix + 'cancel-overflow',
      overlay: prefix + 'overlay',
      box: prefix + 'box',
      close: prefix + 'close'
    };
    this.Selector = {
      overlay: '.' + this.Class.overlay,
      box: '.' + this.Class.box,
      button: '[data-modal=button]'
    };
    this.Markup = {
      close: '<div class=" ' + this.Class.close + ' ">Cerrar X</div>',
      overlay: '<div class=" ' + this.Class.overlay + ' "></div>',
      box: '<div class=" ' + this.Class.box + ' "></div>'
    }
    this.youtubeID = false;
  };

  Modal.prototype = {

    toggleOverflow: function() {
      $('body').toggleClass(this.Class.stopOverflow);
    },

    videoContainer: function() {
      return '<div class="video-container"><iframe id="player" src="https://www.youtube.com/embed/' + this.youtubeID + '?autoplay=1&rel=0" frameborder="0"></iframe></div>';
    },

    addOverlay: function() {

      var self = this;

      $(this.Markup.overlay).appendTo('body').fadeIn('slow', function() {
        self.toggleOverflow();
      });

      $(this.Selector.overlay).on('click touchstart', function() {
        self.closeModal();
      })

    },

    addModalBox: function() {
      $(this.Markup.box).appendTo(this.Selector.overlay);
    },

    buildModal: function(youtubeID) {

      this.addOverlay();
      this.addModalBox();

      $(this.Markup.close).appendTo(this.Selector.overlay);
      $(this.videoContainer(youtubeID)).appendTo(this.Selector.box);

    },

    closeModal: function() {

      this.toggleOverflow();

      $(this.Selector.overlay).fadeOut().detach();
      $(this.Selector.box).empty();

    },

    getYoutubeID: function() {
      return this.youtubeID;
    },

    setYoutubeID: function(href) {

      var id = '';

      if (href.indexOf('youtube.com') > -1) {
        // full Youtube link
        id = href.split('v=')[1];
      } else if (href.indexOf('youtu.be') > -1) {
        // shortened Youtube link
        id = href.split('.be/')[1];
      } else {
        // in case it's not a Youtube link, send them on their merry way
        document.location = href;
      }

      // If there's an ampersand, remove it and return what's left, otherwise return the ID
      this.youtubeID = (id.indexOf('&') != -1) ? id.substring(0, amp) : id;

    },

    startup: function(href) {

      this.setYoutubeID(href);

      if (this.youtubeID) {
        this.buildModal();
      }

    }

  };

  $(document).ready(function() {

    var modal = new Modal();

    $(modal.Selector.button).on('click touchstart', function(e) {
      e.preventDefault();
      modal.startup(this.href);
    });

  });

})(this);
